const express = require("express");
const router = express.Router();
const path = require('path');

const { mongoose } = require("../db/connection");

const { Pony } = require("../models/pony");

router.use(function(req, res, next) {
  var log = req.originalUrl;
  console.log(log);
  next();
});

// HOME
router.get("/", (req, res) => {
  Pony.find().then(ponies => {
    res.render("pages/index", {
      ponies: ponies
    });
  });
});

// HELP
router.get("/help", (req, res) => {
  const title = "Halp!";
  res.render("pages/help", {
    title: title
  });
});

// ABOUT
router.get("/about", (req, res) => {
  const title = "Welcome About";
  res.render("pages/about", {
    title: title
  });
});

// No robots
router.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
});

// No really means no
router.get('*/*.php*', function (req, res, next) {
  console.log('Boooom!');
  res.header('Content-Type', 'text/html');
  res.header('Content-Length', '10180');
  res.header('Content-Encoding', 'gzip');
  res.header('Content-Disposition', 'inline');
  res.sendFile(path.resolve(__dirname + '/../public/secpup.php'));
});

//Not found Page
router.use(function(req, res) {
  console.log('404 - Not found');
  //return res.status(404).send({ success: false, msg: 'Page not found' })
  // just rude now :
  res.header('Content-Type', 'text/html');
  res.header('Content-Length', '10180');
  res.header('Content-Encoding', 'gzip');
  res.header('Content-Disposition', 'inline');
  res.sendFile(path.resolve(__dirname + '/../public/secpup.php'));
});

module.exports = router;
