const express = require("express");
const router = express.Router();
const { mongoose } = require("../db/connection");

const { ensureAuthenticated } = require("../helper/auth");

const { Pony } = require("../models/pony");

//get pony list
router.get("/ponies", ensureAuthenticated, (req, res) => {
  Pony.find({}, null, {sort: 'created_at'})
    .then(ponies => {
      var isAdmin = (req.user.group == 'admin')
      res.render("ponies/index", {
        ponies: ponies,
        isAdmin: isAdmin
      });
    })
    .catch(err => {
      res.send(err);
    });
});

router.get("/heart", ensureAuthenticated, (req, res) => {
  res.redirect("/ponies");
});

router.get("/add/pony", ensureAuthenticated, (req, res) => {
  res.render("ponies/add");
});

router.post("/ponies", ensureAuthenticated, (req, res) => {
  let errors = [];
  if (!req.body.name) {
    errors.push({ text: "Please add a Name" });
  }
  if (!req.body.details) {
    errors.push({ text: "Please add a bio" });
  }
  if (errors.length > 0) {
    res.render("ponies/add", {
      errors: errors,
      name: req.body.name,
      details: req.body.details,
      level: req.body.level,
      age: req.body.age,
      sex: req.body.sex,
      size: req.body.size,
      breed: req.body.breed,
      image: req.body.image
    });
  } else {
    const pony = new Pony({
      name: req.body.name,
      details: req.body.details,
      level: req.body.level,
      age: req.body.age,
      sex: req.body.sex,
      size: req.body.size,
      breed: req.body.breed,
      image: req.body.image,
      creator: req.user.id
    });
    pony
      .save()
      .then(result => {
        req.flash("success_msg", "Pony Added");
        res.redirect("/ponies");
      })
      .catch(err => {
        res.send(err);
      });
  }
});

router.get("/edit/pony/:id", ensureAuthenticated, (req, res) => {
  Pony.findOne({ _id: req.params.id }).then(pony => {
    if (pony.creator != req.user.id) {
      req.flash("error_msg", "Not Authorised");
      res.redirect("/ponies");
    } else {
      res.render("ponies/edit", {
        pony: pony
      });
    }
  });
});

// yes get sorry not sorry
router.get("/delete/pony/:id", ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  Pony.findOneAndRemove({ _id: id }).then(pony => {
    if (!pony) {
      res.send("Not found");
    } else {
      req.flash("error_msg", "Pony Deleted");
      res.redirect("/ponies");
    }
  });
});

router.put("/ponies/:id", ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  Pony.findOne({ _id: id }).then(pony => {
    pony.name = req.body.name;
    pony.details = req.body.details;
    pony.level = req.body.level;
    pony.age = req.body.age;
    pony.sex = req.body.sex;
    pony.size = req.body.size;
    pony.breed = req.body.breed;
    pony.image = req.body.image;
    pony
      .save()
      .then(pony => {
        req.flash("success_msg", "Pony Updated");
        res.redirect("/ponies");
      })
      .catch(err => {
        req.send(err);
      });
  });
});

module.exports = router;
