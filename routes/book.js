const express = require("express");
const router = express.Router();
const { mongoose } = require("../db/connection");
const { ensureAuthenticated } = require("../helper/auth");
const { Event } = require("../models/book");

//get event list
router.get("/events", ensureAuthenticated, (req, res) => {
  Event.find()
    .then(events => {
      var appointments = events.filter(entry => { return entry.type == 'appointment'; });
      var lessons = events.filter(entry => { return entry.type == 'lesson'; });
      var reminders = events.filter(entry => { return entry.type == 'reminder'; });

      //console.log(JSON.stringify(lessons));
      res.render("events/index", {
        appointments: appointments,
        lessons: lessons,
        reminders: reminders
      });
    })
    .catch(err => {
      console.log('Error', err);
      res.send(err);
    });
});

router.get("/add/event", ensureAuthenticated, (req, res) => {
  res.render("events/add", {
    isLesson: true
  });
});

router.post("/events", ensureAuthenticated, (req, res) => {
  let errors = [];
  if (!req.body.title) {
    errors.push({ text: "Please add a Title" });
  }
  if (!req.body.start) {
    errors.push({ text: "Please add when " });
  }
  if (errors.length > 0) {
    res.render("events/add", {
      errors: errors,
      title: req.body.title,
      start: req.body.start,
      end: req.body.end,
      instructor: req.body.instructor,
      location: req.body.location
    });
  } else {
    const event = new Event({
      title: req.body.title,
      type: 'lesson',
      start: req.body.start,
      end: req.body.end,
      instructor: req.body.instructor,
      location: req.body.location,
      creator: req.user.id
    });
    event
      .save()
      .then(result => {
        req.flash("success_msg", "Event Added");
        res.redirect("/events");
      })
      .catch(err => {
        res.send(err);
      });
  }
});

router.get("/edit/event/:id", ensureAuthenticated, (req, res) => {
  Event.findOne({ _id: req.params.id }).then(event => {
    if (event.creator != req.user.id) {
      req.flash("error_msg", "Not Authorised");
      res.redirect("/events");
    } else {
      res.render("events/edit", {
        event: event,
        isLesson: (event.type == 'lesson')
      });
    }
  });
});

// yes get sorry not sorry
router.get("/delete/event/:id", ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  Event.findOneAndRemove({ _id: id }).then(event => {
    if (!event) {
      res.send("Not found");
    } else {
      req.flash("error_msg", "Event Deleted");
      res.redirect("/events");
    }
  });
});

router.put("/events/:id", ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  Event.findOne({ _id: id }).then(event => {
    event.title = req.body.title;
    event.type = req.body.type || 'lesson';
    event.start = req.body.start;
    event.end = req.body.end;
    event.instructor = req.body.instructor;
    event.location = req.body.location;
    event.creator =  req.user.id
    event
      .save()
      .then(event => {
        req.flash("success_msg", "Event Updated");
        res.redirect("/events");
      })
      .catch(err => {
        console.log(event, err);
        req.send(err);
      });
  });
});

// stripe tut
const keyPublishable = process.env.PUBLISHABLE_KEY;
const keySecret = process.env.SECRET_KEY;
const stripe = require("stripe")(keySecret);

router.get("/book", ensureAuthenticated, (req, res) => {
  res.render("events/pay");
});

// stripe tut

router.post("/charge", (req, res) => {
  let amount = 3000;

  stripe.customers.create({
    email: req.body.email,
    card: req.body.id
  })
  .then(customer =>
    stripe.charges.create({
      amount,
      description: "Book a lesson",
      currency: "gbp",
      customer: customer.id
    }))
  .then(charge => res.send(charge))
  .catch(err => {
    console.log("Error:", err);
    res.status(500).send({error: "Purchase Failed"});
  });
});

module.exports = router;
