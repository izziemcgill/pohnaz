const express = require("express");
const app = express();
//const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const path = require("path");
const session = require("express-session");
const methodOverride = require("method-override");
const flash = require("connect-flash");
const passport = require("passport");

const idea = require("./routes/idea");
require("./config/passport")(passport);

const port = process.env.PORT || 3000;

// handlebars middleware
var hbs = require('express-hbs');
hbs.registerHelper('dateFormat', require('handlebars-dateformat'));

app.engine('hbs', hbs.express4({
  defaultLayout: __dirname + '/views/layouts/base',
  partialsDir: __dirname + '/views/partials'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

//method override middleware
app.use(methodOverride("_method"));

//Flash message middleware
app.use(
  session({
    secret: "Secret",
    resave: true,
    saveUninitialized: true
  })
);

//passport middleware
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Set Global Variable
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//Set Static Path
app.use(express.static(path.join(__dirname, "public")));

//imports Routes
app.use(require("./routes/idea"));
app.use(require("./routes/pony"));
app.use(require("./routes/book"));
app.use(require("./routes/user"));
app.use(require("./routes/page"));

app.listen(port, () => {
  console.log(`server is running on port ${port}`);
});
