    document.onkeydown = (event) => {
      var e = event || window.event;
      var key = e.which || e.keyCode;

      if(key === 33) { // Page up
        window.scrollBy(0, -250)
      }

      if (key === 34) { // Page down
        window.scrollBy(0, 250)
      }
    };

