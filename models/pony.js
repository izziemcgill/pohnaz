const mongoose = require("mongoose");
const ponySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  breed: {
    type: String,
    default: 'Thoroughbred',
    required: true
  },
  sex: {
    type: String,
    required: true
  },
  age: {
    type: Number
  },
  size: {
    type: String,
    required: true
  },
  level: {
    type: String,
    required: true
  },
  details: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: 'fluffy.png'
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  creator: {
    type: String,
    required: true
  }
});
const Pony = mongoose.model("Pony", ponySchema);
module.exports = { Pony };
