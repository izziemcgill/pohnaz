const mongoose = require("mongoose");
const EventSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  start: {
    type: String,
    required: true
  },
  end: {
    type: String
  },
  instructor: {
    type: String
  },
  location: {
    type: String
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  creator: {
    type: String,
    required: true
  }
});
const Event = mongoose.model("Event", EventSchema);
module.exports = { Event };
